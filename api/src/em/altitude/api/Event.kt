package em.altitude.api

import em.altitude.api.model.*

/**
 * A sporadic event that `Logic` should react to.
 */
sealed class Event {
    class Join(val client: Client) : Event()

    class PlayerChange(val client: Client) : Event()

    class Leave(val client: Client, val reason: String?, val Message: String?) : Event()

    class Spawn(player: Int): Event()

    class PowerupGet(player: Int, powerup: Powerup, used: Boolean, defused: Boolean): Event()

    class PowerupUse(player: Int, powerup: Powerup): Event()

    class MapChange(): Event()

    class Despawn(player: Int, stats: Client.Stats): Event()

    class Kill(player: Int, victim: Int, assists: Array<Int>): Event()

    class RoundEnd(): Event()

    class Damage(player: Int, target: Structure, damage: Float, destroyed: Boolean): Event()

    class Goal(player: Int, assists: Array<Int>): Event()

    class BallLost(): Event()

    class Chat(val message: String, val team: Boolean) : Event()
}
