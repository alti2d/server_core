package em.altitude.api

import em.altitude.api.model.Game
import em.altitude.api.Event

/**
 * The top-level interface a mod implements with `em.altitude.core.LogicImpl`.
 */
abstract class Logic(
        val game: Game
) {
    abstract fun tick()
    abstract fun handle(event: Event)
}

