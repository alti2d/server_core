package em.altitude.api.model

interface BanList {
    interface Descriptor {
        val id: String
        val start: Long
        val end: Long
        val nickname: String
        val reason: String
    }

    fun add(id: String, descriptor: Descriptor)
    fun get(id: String): Descriptor
    fun remove(id: String)

    val list: Collection<Descriptor>
}