package em.altitude.api.model

import java.util.UUID

/**
 * A single client, having successfully joined the game.
 */
interface Client {
    interface ClientID {
        val pid: Int
        val nickname: String
        val vaporId: UUID
        val playTime: Long
    }

    /** Restricts certain properties of a player to a set of values given by the server. */
    interface Restrictions {
        var team: Set<Team>
        var redPerks: Set<RedPerk>
        var bluePerks: Set<BluePerk>
        var greenPerks: Set<GreenPerk>
        /** Attempt to fix a player's perks to allow them to spawn */
        var fixPerks: Boolean
    }

    /** Player statistics */
    interface Stats {
        var kills: Int
        var deaths: Int
        var crashes: Int
        var assists: Int
        var damageDealt: Float
        var damageTaken: Float
        var structureDamage: Float
        var longestLife: Int
        var killStreak: Int
        var multikill: Int
        var experience: Int
        var goalsScored: Int
        var goalsAssisted: Int
        var ballPossessionTime: Int
    }

    //// State ////

    val loading: Boolean
    val id: ClientID
    val team: Team
    val plane: Plane?
    val setup: Plane.Setup
    val level: Int
    val ace: Int
    val stats: Stats


    /// Server-authoritative settings ///

    val restrictions: Restrictions
    val specChat: Boolean
    /* ... */


    /// Methods ///

    /** Send a server message to the player */
    fun whisper(message: String)

    /** Remove the player from the server. */
    fun drop(message: String, broadcast: String)

    /** Move the player to the given team */
    fun assignTeam(team: Team)

    /** Move the player to a new server */
    fun requestChangeServer(address: String, password: String?)
}

