package em.altitude.api.model

/**
 * Our interface to the underlying game server.
 *
 * In addition to some effectful methods, many properties are
 * exposed. Only a few of these are settable, corresponding to
 * server-authoritative settings.
 */
interface Game {
    //// State ////

    val clients: Collection<Client>
    val bots: BotManager
    val bans: BanList
    val mutes: BanList
    val teamMutes: BanList
    val properties: Properties
    val map: String
    val mode: GameMode
    val teams: Collection<Team>
    val leftTeam: Team?
    val rightTeam: Team?
    val currentVote: VoteState?


    //// Methods ////

    /** Change map. Returns false if map cannot be loaded. */
    fun changeMap(newMap: String): Boolean

    /** End the round, showing the score screen */
    fun endRound()

    /** Remove the current ball, replacing it with a new one at the given position */
    fun resetBall(x: Float, y: Float)

    /** Remove the current ball, replacing it with a new one at the normal spawn
     *  point for a given team */
    fun resetBallTeam(team: Team)

    /** Send the 'Ball Lost' message, de-spawning all clients. */
    fun sendBallLost()

    /** Send a chat message to all players */
    fun serverMessage(message: String)

    /** Start a new vote */
    fun startVote(title: String, message: String, options: Array<String>)

    /** End the current vote */
    fun endVote(message: String)

    /** Take a lock on the game state, for safely accessing it from other threads */
    fun lock()

    /** Release a lock on the game state */
    fun unlock()
}