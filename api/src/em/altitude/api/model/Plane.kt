package em.altitude.api.model

interface Plane {
    val player: Int
    val setup: Setup
    val pos: Vector
    val vel: Vector
    val angle: Float
    val team: Team

    /** A plane's configuration */
    data class Setup(
            val random: RandomType,
            val redPerk: RedPerk,
            val greenPerk: GreenPerk,
            val bluePerk: BluePerk,
            val skin: Skin
    )

    /** Change the plane's linear momentum */
    fun push(x: Float, y: Float)
}

