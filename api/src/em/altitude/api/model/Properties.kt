package em.altitude.api.model

interface Properties {
    var energyMod: Float
    var healthMod: Float
    var disableSpawn: Boolean
    var crashDamageMod: Float
    var elasticityMod: Float
    var viewScale: Float
    var planeScale: Float
    var disablePrimary: Boolean
    var disableSecondary: Boolean
    var gravity: Boolean
    var ballGravity: Boolean
    var voteCooldown: Int
    var voteFailCooldown: Int
    var autoStopTournament: Boolean
    var specChat: Boolean
    /** Use tournament teams for team chat. */
    var tournamentTeamChat: Boolean
    /** Enforce a maximum number of players in each team */
    val enforceTeamSize: Boolean
    /** The maximum number of players per team */
    var teamSize: Int

}
