package em.altitude.api.model

interface Vector {
    val x: Float
    val y: Float
}

// TODO: make the JSON bridge understand teams
enum class Team {
    A, B, Spectator,
    Red,
    Blue,
    Green,
    Yellow,
    Orange,
    Purple,
    Azure,
    Pink,
    Brown,
}

enum class RandomType {
    None, Custom, Full
}

enum class RedPerk {
    Tracker, DoubleFire, Acid,
    Suppressor, Bombs, Flak,
    Director, Thermo, Remote,
    Dogfighter, Recoilless, HeavyCannon,
    Trickster, Laser, TimeAnchor,
}

enum class GreenPerk { None, Rubber, Heavy, Repair, Flexi }

enum class BluePerk { None, Turbo, Ultra, Reverse, Ace }

enum class Skin { None, Shark, Zebra, Checker, Flame, Santa }

enum class Powerup {
    Health, Shield, Wall, Missile, Bomb, Charge, Ball
}

enum class GameMode {
    TeamBaseDestruction,
    OneLifeBaseDestruction,
    OneLifeDemolition,
    OneLifeDeathmatch,
    TeamDeathmatch,
    FreeForAll,
    Ball,
}

data class Structure(val pos: Vector, val kind: StructureKind)

enum class StructureKind{ Turret, Base }
