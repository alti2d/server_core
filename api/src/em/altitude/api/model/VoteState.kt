package em.altitude.api.model

/**
 * The state of a running vote. This is visible by the client, and used
 * by our code to define the terminal conditions for a vote
 */
interface VoteState {
    /** The collection of clients who have voted for each option */
    // TODO: Safe way to make sure no disconnection clients stay here?
    val votes: List<Collection<Client>>

    // Also: some indication on client-side of how many votes are needed
    // to fail or succeed.
}

