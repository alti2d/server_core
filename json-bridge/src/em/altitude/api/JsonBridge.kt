package em.altitude.api

import jdk.nashorn.internal.runtime.JSONFunctions
import org.json.simple.JSONArray
import org.json.simple.JSONAware
import org.json.simple.JSONObject
import org.json.simple.JSONValue
import org.json.simple.parser.JSONParser
import java.lang.reflect.Modifier
import kotlin.reflect.*
import kotlin.reflect.full.*
import kotlin.reflect.jvm.javaGetter
import kotlin.reflect.jvm.javaMethod

/**
 * Call types.
 */

private sealed class Command() {
}

private class Get(val get: String) : Command()
private class Set(val set: String, val to: Any) : Command()
private class Call(val method: String, val with: Array<Any>) : Command()

// TODO: Better error catching here...?
@Suppress("UNCHECKED_CAST")
private fun parseCommand(command: String): Command? {
    try {
        val parsed = JSONParser().parse(command) as JSONObject
        if (parsed["get"] != null) {
            return Get(parsed["get"] as String)
        } else if (parsed["set"] != null && parsed["to"] != null) {
            return Set(parsed["set"] as String, parsed["to"] as Any)
        } else if (parsed["method"] != null && parsed["with"] != null) {
            return Call(parsed["method"] as String, (parsed["with"] as JSONArray).toArray() as Array<Any>)
        } else {
            return null
        }
    } catch (e: Exception) {
        print("crap: ${e.message}")
        return null
    }
}

/**
 * Response types.
 */

private sealed class Response() {
    abstract val json: JSONObject
}

private class Got(val got: String, val value: JSONAware) : Response() {
    override val json
        get() = run {
            val obj = JSONObject(); obj["got"] = got; obj["value"] = value; obj
        }
}

private class Return(val method: String, val value: JSONAware) : Response() {
    override val json
        get() = run {
            val obj = JSONObject(); obj["method"] = method; obj["value"] = value; obj
        }
}

private class SyntaxError(val command: String) : Response() {
    override val json
        get() = run {
            val obj = JSONObject(); obj["error"] = "syntax"; obj["command"] = command; obj
        }
}

private class ProtocolError(val explanation: String) : Response() {
    override val json
        get() = run {
            val obj = JSONObject(); obj["error"] = "protocol"; obj["explanation"] = explanation; obj
        }
}

/**
 * Abstract utility to access properties and methods of a class and its members through a JSON interface.
 */
class JsonCaller<T : Any>(val c: KClass<T>) {
    var methods = HashMap<String, (T, Array<JSONObject>) -> JSONAware>()
    var getters = HashMap<String, (T) -> JSONAware>()
    var setters = HashMap<String, (T, JSONObject) -> Unit>()

    init {
        populateFrom("", c)
    }

    private fun <T : Any> populateFrom(path: String, clazz: KClass<T>) {
        // Look at methods.
        for (method in clazz.functions) {
            print("Inspecting method ${method.name}...\n")
            val modifiers = method.javaMethod?.modifiers
            if (modifiers != null && Modifier.isPublic(modifiers)) {
                // TODO: more robust elimination of superclass methods we don't care about.
                if (method.name in setOf(
                        "getFunctions", "getLocalProperty", "getProperties",
                        "hashCode", "isInstance", "toString", "equals")) continue

                // We have a public method. We can add it as long as it returns a JSONAware value.
                if (method.returnType.isSupertypeOf(JSONAware::class.createType())) {
                    print("Method return type (${method.returnType.toString()}) is JSONAware")
                    methods[path + method.name] = { instance, args ->
                        method.call(this, *args) as JSONAware
                    }
                }
            }
        }

        // TODO: properties
        // Look at properties.
        for (prop in c.declaredMemberProperties) {
            val modifiers = prop.javaGetter?.modifiers
            // We have a public property. If it's a simple type, add it; otherwise, recurse
            // `populateFrom` into it.
            print("Inspecting property ${prop.name}...\n")
            /*
            if (isSimpleType(prop .returnType)) {
                ...
                if (prop is KMutableProperty<member.returnType>) {// Does this work?!
                    ...
                }
            } else {
                ...
            }
            */
        }
    }

    private fun handleCommand(instance: T, command: Command): Response? {
        return when (command) {
            is Get -> {
                val getter = getters[command.get]
                if (getter != null) {
                    Got(command.get, getter(instance))
                } else {
                    ProtocolError("${"a"} is not a valid property in " +
                            "${instance::class.simpleName}.")
                }
            }

            is Set -> {
                val setter = setters[command.set]
                if (setter != null) {
                    setter(instance, command.to as JSONObject)
                    null
                } else {
                    ProtocolError("${command.set} is not a valid mutable in " +
                            "${instance::class.simpleName}.")
                }
            }

            is Call -> {
                val method = methods[command.method]
                return if (method != null) {
                    @Suppress("UNCHECKED_CAST")
                    Return(command.method, method(instance, command.with as Array<JSONObject>))
                } else {
                    ProtocolError("${command.method} is not a valid method of " +
                            "${instance::class.simpleName}.")
                }
            }
        }
    }

    fun call(instance: T, command: String): String? {
        val parsed = parseCommand(command)
        return if (parsed != null) {
            handleCommand(instance, parsed)?.json?.toJSONString()
        } else {
            SyntaxError(command).json.toJSONString()
        }
    }
}
