package test

import em.altitude.api.model.Client
import org.junit.Test
import em.altitude.api.model.Game
import em.altitude.core.JsonCaller
import em.altitude.model.Properties
import kotlin.test.assertEquals

data class MyData(val thingy: Int)

interface MyInterface {
    val readable: MyData
    var writable: String
    fun doAction(x: Int)
    val calculator: Calculator
}

class Calculator {
    fun calculateThing(x: Int) = x + 1
}

class MyClass(var list: MutableList<Int>) : MyInterface {
    override val readable = MyData(42)

    override var writable: String = "Change me!"

    override fun doAction(x: Int) {
        list.add(x)
    }

    override val calculator = Calculator()
}

class CallerTest() {
    var caller = JsonCaller(MyInterface::class)
    var list = mutableListOf<Int>()
    var instance = MyClass(list)

    @Test
    fun reading() {
        val response = caller.call(instance,
                """ { get: "readable.thingy" } """)
        assertEquals(""" { got: "readable.thingy", value: 42 }""", response)
    }

    @Test
    fun writing() {
        val response1 = caller.call(instance,
                """ { set: "writable", to: "Okay, done." } """)
        assertEquals(null, response1)
        val response2 = caller.call(instance,
                """ { "get": "writable" } """)
        assertEquals("""{"got":"writable","value":"Okay, done".}""", response2)
    }

    @Test
    fun invoking() {
        list = mutableListOf()
        val response = caller.call(instance,
                """ {"method":"doAction","with":[1]}""")
        assertEquals(null, response)
        assertEquals(1, list.size)
        assertEquals(42, list[0])
    }

    fun returning() {
        val response = caller.call(instance,
                """ { "call": "calculator.calculate", "with": [41] } """)
        assertEquals(""" { "return": "calculator.calculate", "value": 42 }""", response)
    }
}